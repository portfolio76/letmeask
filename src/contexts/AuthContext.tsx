import { useEffect, ReactNode, useState, createContext } from "react";

import { auth, firebase } from '../services/firebase';

type AuthContextType = {
  user: User | undefined;
  signInWithGoogle: () => Promise<void>;
  logOut: () => void;
}

type User = {
  id: string;
  name: string;
  avatar: string;
}

type AuthContextProviderProps = {
  children: ReactNode;
}

export const AuthContext = createContext({} as AuthContextType);

export function AuthContextProvider({ children }: AuthContextProviderProps) {
  const [user, setUSer] = useState<User>();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const unSubscribe = auth.onAuthStateChanged(user => {
      if(user) {
        const { displayName, photoURL, uid } = user;
        if(!displayName || !photoURL) throw new Error("Missing information from Google Account.");
        setUSer({
          id: uid,
          name: displayName,
          avatar: photoURL
        });
        setLoading(false);
      }
    });
    return () => {
      unSubscribe();
    };
  }, []);

  async function signInWithGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider();
    const result = await auth.signInWithPopup(provider)
    if(result.user) {
      const { displayName, photoURL, uid } = result.user;
      if(!displayName || !photoURL) throw new Error("Missing information from Google Account.");
      setUSer({
        id: uid,
        name: displayName,
        avatar: photoURL
      });
    }
  };

  async function logOut() {
    await auth.signOut();
    setUSer({} as User);
  }

  if(loading) return (
    <p className="loading">
      Loading...
    </p>
  );
  
  return (
    <AuthContext.Provider value={{ user, signInWithGoogle, logOut }}>
      {children}
    </AuthContext.Provider>
  );
};