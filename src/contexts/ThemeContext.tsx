import { useEffect } from "react";
import { createContext, ReactNode, useState } from "react";

type Theme = 'light' | 'dark';
type ThemeContextProviderProps = {
  children: ReactNode;
};
type ThemeContextType = {
  theme: Theme;
  toggleTheme: () => void;
};
export const ThemeContext = createContext<ThemeContextType>({} as ThemeContextType);

export function ThemeContextProvider({ children }: ThemeContextProviderProps) {
  const [theme, setTheme] = useState<Theme>(
    () => {
      const storageTheme = localStorage.getItem('theme');
      return (storageTheme ?? 'light') as Theme;
    }
  );

  useEffect(() => {
    localStorage.setItem('theme', theme);
  }, [theme]);

  function toggleTheme() {
    setTheme(theme === 'light' ? 'dark' : 'light');
  };

  return (
    <ThemeContext.Provider
      value={{ theme, toggleTheme }}
    >
      {children}
    </ThemeContext.Provider>
  );
};