import { FormEvent, useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';

import { Button } from '../components/Button';
import { useAuth } from '../hooks/useAuth';
import { useTheme } from '../hooks/useTheme';

import { database } from '../services/firebase';

import illustrationImg from '../assets/images/illustration.svg';
import logoImg from '../assets/images/logo.svg';
import logoDarkImg from '../assets/images/logo-dark.svg';

import '../styles/auth.scss';

export function NewRoom() {
  const [newRoom, setNewRoom] = useState('');

  const history = useHistory();
  const { user } = useAuth();
  const { theme, toggleTheme } = useTheme();

  useEffect(() => {
    if(!user) {
      alert('You must be logged in');
      history.push('/');
    }
  }, [user, history]);

  async function handleCreateRoom(event: FormEvent) {
    event.preventDefault();

    if(newRoom.trim() === '') return;

    const roomRef = database.ref('rooms');
    const firebaseRoom = await roomRef.push({
      title: newRoom,
      authorId: user?.id
    });

    history.push(`/rooms/${firebaseRoom.key}`);
  };

  return(
    <div id="page-auth" className={theme}>
      <aside>
        <img src={illustrationImg} alt="Illustration" />
        <strong>Create Q&amp;A rooms online</strong>
        <p>Remove doubts from your audience in real-time</p>
      </aside>
      <main>
        <div className="main-content">
          <button
            onClick={toggleTheme}
          >
            {theme}
          </button>

          <img src={theme === 'light' ? logoImg : logoDarkImg} alt="Logo" />

          <span className="greetings">
            {user ? (`Hi ${user?.name}`) : ('')}
          </span>
          <h2>Create the new room</h2>
          <form onSubmit={handleCreateRoom}>
            <input
              type="text"
              placeholder="Room's name"
              onChange={event => setNewRoom(event.target.value)}
              value={newRoom}
            />
            <Button type="submit"> Create room </Button>
          </form>
          <p>
            Do you want to go into the existent room? <Link to="/">click here</Link>
          </p>
        </div>
      </main>
    </div>
  );
};