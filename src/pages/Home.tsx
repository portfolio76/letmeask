import { FormEvent, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { Button } from '../components/Button';
import { useAuth } from '../hooks/useAuth';
import { useTheme } from '../hooks/useTheme';

import { database } from '../services/firebase';

import illustrationImg from '../assets/images/illustration.svg';
import logoImg from '../assets/images/logo.svg';
import logoDarkImg from '../assets/images/logo-dark.svg';
import googleIconImg from '../assets/images/google-icon.svg';

import '../styles/auth.scss';

type FirebaseQuestion = Record<string, {
  author: {
    name: string;
    avatar: string;
  };
  content: string;
  isAnswered: boolean;
  isHighlighted: boolean;
  likes: Record<string, {
    authorId: string;
  }>;
}>;

type FirebaseRoom = {
  authorId: string;
  questions: FirebaseQuestion;
  title: string;
};

export function Home() {
  const [roomTitle, setRoomTitle] = useState('');

  const { user, signInWithGoogle } = useAuth();
  const history = useHistory();
  const { theme, toggleTheme } = useTheme();

  async function handleCreateRoom() {
    if(!user) await signInWithGoogle();
    history.push('/rooms/new');
  };

  async function handleJoinRoom(event: FormEvent) {
    event.preventDefault();
    if(roomTitle.trim() === '') return;

    let roomCode = undefined;
    await database.ref(`rooms`).get().then((snapshot) => {
      if (snapshot.exists()) {
        const rooms = Object.entries(snapshot.val());
        const parsedRooms = rooms as [string, FirebaseRoom][];
        const room = parsedRooms.find(r => r[1].title === roomTitle);
        if(room) roomCode = room[0];
        else{
          alert('Room does not exists.');
          return;
        }
        console.log(roomCode);
      }
      else alert("No data available");
    }).catch((error) => {
      alert(error);
    });

    if(roomCode) {
      const roomRef = await database.ref(`rooms/${roomCode}`).get();
      if(roomRef.val().closedAt) {
        alert('Room already closed.');
        return;
      }

      history.push(`/rooms/${roomCode}`);
    }
  }

  return(
    <div id="page-auth" className={theme}>
      <aside>
        <img src={illustrationImg} alt="Illustration" />
        <strong>Create Q&amp;A rooms online</strong>
        <p>Remove doubts from your audience in real-time</p>
      </aside>
      <main>
        <div className="main-content">
          <button
            onClick={toggleTheme}
          >
            {theme}
          </button>

          <img src={theme === 'light' ? logoImg : logoDarkImg} alt="Logo" />

          <span className="greetings">
            {user ? (`Hi ${user?.name}`) : ('')}
          </span>

          <button onClick={handleCreateRoom} className="create-room">
            {!user ? (<img src={googleIconImg} alt="Google logo" />) : (null)}
            Create a new room {!user ? (' with Google') : ('')}
          </button>

          <div className="separator"> or enter to a created room</div>
          <form>
            <input
              type="text"
              placeholder="Write the room name"
              onChange={event => setRoomTitle(event.target.value)}
              value={roomTitle}
            />
            <Button type="submit" onClick={handleJoinRoom}> Go into the room </Button>
          </form>
        </div>
      </main>
    </div>
  )
};